from typing import List
from domain.todo import Todo
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import Mapped
from sqlalchemy import select
from sqlalchemy import String
from sqlalchemy import Boolean
from sqlalchemy.orm import Session
from store.db_engine import get_engine

class Base(DeclarativeBase):
    pass

class TodoDb(Base):
     __tablename__ = "todo"

     id: Mapped[str] = mapped_column(primary_key=True)
     content: Mapped[str] = mapped_column(String(30))
     completed: Mapped[bool] = mapped_column(Boolean, default=False)

def map_todo_to_domain(todo_db: TodoDb):
    return Todo(todo_db.id, todo_db.content, todo_db.completed)

def map_todo_to_db(todo: Todo):
    todo_db = TodoDb()
    todo_db.id = todo.id
    todo_db.content = todo.content
    todo_db.completed = todo.completed
    return todo_db

class TodoStore:

    def __init__(self):
        self.engine = get_engine(Base.metadata.create_all)

    def add_todo(self, todo: Todo) -> Todo:
        with Session(self.engine) as session:
            todo_db = map_todo_to_db(todo)
            session.add(todo_db)
            session.commit()
        return todo

    def get_todo(self) -> List[Todo]:
        with Session(self.engine) as session:
            stmt = select(TodoDb)
            return [map_todo_to_domain(todo_db) for todo_db in session.scalars(stmt)]
