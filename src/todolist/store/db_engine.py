from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
import os, sys

database_url = os.environ.get('TODO_DB_URL', "sqlite://")

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

this.engine: Engine = None

def get_engine(callback) -> Engine:
    if not this.engine:
      this.engine = create_engine(database_url, echo=True)
      callback(this.engine)
    return this.engine
